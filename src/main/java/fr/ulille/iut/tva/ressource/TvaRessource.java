package fr.ulille.iut.tva.ressource;

import java.util.ArrayList;
import java.util.List;

import fr.ulille.iut.tva.dto.InfoTauxDto;
import fr.ulille.iut.tva.service.CalculTva;
import fr.ulille.iut.tva.service.TauxTva;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.QueryParam;
import jakarta.ws.rs.core.MediaType;
import jakarta.xml.bind.annotation.XmlRootElement;

/**
 * TvaRessource
 */
@Path("tva")
public class TvaRessource {
	private CalculTva calculTva = new CalculTva();


	@GET
	@Path("tauxpardefaut")
	public double getValeurTauxParDefaut() {
		try {
			return TauxTva.NORMAL.taux;			
		} catch (Exception e) {
			throw new NiveauTvaInexistantException();
		}
	}

	@GET
	@Path("valeur/{niveauTva}")
	public double getValeurTaux(@PathParam("niveauTva") String niveau) {
		try {
			return TauxTva.valueOf(niveau.toUpperCase()).taux;		
		} catch (Exception e) {
			throw new NiveauTvaInexistantException();
		}
	}


	@GET
	@Path("/{montantTotal}")
	public double getMontantTotal(@PathParam("montantTotal") String niveau, @QueryParam("somme") int somme) {
		return (new CalculTva()).calculerMontant(TauxTva.valueOf(niveau.toUpperCase()) , somme);
	}
	
    @GET
    @Path("lestaux")
    public List<InfoTauxDto> getInfoTaux() {
      ArrayList<InfoTauxDto> result = new ArrayList<InfoTauxDto>();
      for ( TauxTva t : TauxTva.values() ) {
        result.add(new InfoTauxDto(t.name(), t.taux));
      }
      return result;
    }
    
    @GET
    @Path("details/{taux}")
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    public List<InfoTauxDto> getDetail(@PathParam("taux") String niveau, @QueryParam("somme") int somme) {
		List<InfoTauxDto> res = new ArrayList<InfoTauxDto>();
		double montantTotal =  (new CalculTva()).calculerMontant(TauxTva.valueOf(niveau.toUpperCase()) , somme);
		res.add(new InfoTauxDto("Montant total", montantTotal));
		double montantTVA = somme * (TauxTva.valueOf(niveau.toUpperCase()).taux / 100);
		res.add(new InfoTauxDto("Montant TVA: " , montantTVA));
		//res.add(new InfoTauxDto("tauxLabel: " , TauxTva.valueOf(niveau.toUpperCase())    ))    ;
		res.add(new InfoTauxDto("tauxValue:: " , TauxTva.valueOf(niveau.toUpperCase()).taux));

    	return res;
    }

}
